---
layout: post
title:  "Arrancamos"
date:   2023-01-30 15:32:14 -0300
categories: notas
---
*El espacio: la última frontera. Estos son los viajes de la nave estelar «Enterprise», en una misión que durará cinco años, dedicada a la exploración de mundos desconocidos, al descubrimiento de nuevas vidas y nuevas civilizaciones, hasta alcanzar lugares donde nadie ha podido llegar.*

![Una imagen de un albatros](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b8/Diomedea_epomophora_in_flight_6_-_SE_Tasmania.jpg/2560px-Diomedea_epomophora_in_flight_6_-_SE_Tasmania.jpg "albatros")

Viaje sin retorno

el llamado a la aventura (o el empujón)
distancias abismales
nuevos mundos
nuevos comienzos
el legado

qué tienen en común la literatura de ciencia ficción y el western? las fronteras con lo desconocido, las distancias abismales, las oportunidades de otras vidas

las emociones que producen las nuevas tecnologías (surcar mares/desiertos/galaxias) construir/comenzar desde cero, defenderse de amenazas,

tiempos inconmensurables
lo desconocido, “uncharted territories”

un viaje que compromete la vida, el intercomunicador que falla, se pierde, es robado, no se usa

encontrar el amor en un nuevo mundo, fabricar armas, tener un taller mecánico.

el diario de viaje/la bitácora/blog?
