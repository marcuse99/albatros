---
layout: page
title: Acerca de
permalink: /acerca-de/
---

*El espacio: la última frontera. Estos son los viajes de la nave estelar «Enterprise», en una misión que durará cinco años, dedicada a la exploración de mundos desconocidos, al descubrimiento de nuevas vidas y nuevas civilizaciones, hasta alcanzar lugares donde nadie ha podido llegar.*
